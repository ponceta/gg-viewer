interface ILayerWithLegend {
  legend: boolean;
  legendImage?: string;
  isLegendExpanded: boolean;
  wasLegendExpanded: boolean;
}

export default ILayerWithLegend;
