import { defineConfig } from 'vite';
import { resolve } from 'path';

import InlineTemplatesPlugin from './buildtools/vite-inline-templates-plugin';
import RestartPlugin from './buildtools/vite-restart-plugin';
import { viteStaticCopy } from 'vite-plugin-static-copy';
import basicSsl from '@vitejs/plugin-basic-ssl';
import dns from 'dns';

/**
 * Custom name resolution for app.localhost:
 * Vite try to do a DNS-Lookup when starting the dev server
 * and app.localhost is not defined anywhere.
 * We do not want to define on each client,
 * so the default dns lookup method is overriden
 * to resolve app.localhost to 127.0.0.1
 */
const originalDnsLookup = dns.lookup;
function customDnsLookup(hostname, callback) {
  if (hostname === 'app.localhost') {
    callback(null, '127.0.0.1', 4);
  } else {
    originalDnsLookup(hostname, callback);
  }
}
dns.lookup = customDnsLookup;

// This is the base url for static files that CesiumJS needs to load.
// Set to an empty string to place the files at the site's root path
const cesiumSource = 'node_modules/cesium/Build/Cesium';
// Default configuration for Cesium (see https://cesium.com/learn/cesiumjs-learn/cesiumjs-quickstart/)
const cesiumBaseUrl = 'lib/cesium/';

// https://v2.vitejs.dev/config/
export default defineConfig({
  base: './',
  server: {
    host: 'app.localhost',
    port: 8080,
    strictPort: true
  },
  build: {
    outDir: 'dist/app',
    sourcemap: true,
    emptyOutDir: true,
    rollupOptions: {
      input: {
        desktop: resolve(__dirname, 'index.html'),
        mobile: resolve(__dirname, 'mobile.html')
      },
      output: {
        manualChunks: {
          lazy: [
            '@geoblocks/mapfishprint',
            '@geoblocks/print',
            'buffer',
            'd3',
            'error-stack-parser',
            'file-saver',
            'tabulator-tables',
            'lz-string',
            'source-map-js',
            'tippy.js',
            'vanilla-picker'
          ]
        }
      }
    }
  },
  optimizeDeps: {
    include: ['cesium', 'olcs/OLCesium']
  },
  plugins: [
    viteStaticCopy({
      targets: [
        { src: `${cesiumSource}/ThirdParty`, dest: cesiumBaseUrl },
        { src: `${cesiumSource}/Workers`, dest: cesiumBaseUrl },
        { src: `${cesiumSource}/Assets`, dest: cesiumBaseUrl },
        { src: `${cesiumSource}/Widgets`, dest: cesiumBaseUrl },
        { src: 'service-worker.js', dest: '' },
        { src: 'src/styles/*.css', dest: 'styles/' },
        { src: 'node_modules/ol/ol.css', dest: 'lib/ol/' },
        { src: 'node_modules/tabulator-tables/dist/css/tabulator.min.css', dest: 'lib/tabulator-tables/' },
        { src: 'node_modules/font-gis/css/*.css', dest: 'lib/font-gis/' },
        { src: 'node_modules/font-gis/fonts/*', dest: 'lib/fonts/' },
        { src: 'node_modules/tippy.js/dist/*.css', dest: 'lib/tippy.js/' },
        { src: 'node_modules/vanilla-picker/dist/*.css', dest: 'lib/vanilla-picker/' }
      ]
    }),
    InlineTemplatesPlugin(),
    RestartPlugin(),
    basicSsl()
  ],
  define: {
    // Define relative base path in cesium for loading assets
    // https://vitejs.dev/config/shared-options.html#define
    CESIUM_BASE_URL: JSON.stringify(cesiumBaseUrl)
  }
});
